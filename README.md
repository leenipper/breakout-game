# breakout-game

This game is a webasm based game which uses the gweb API from
https://github.com/life4/gweb.

The source code of this game was originally copied from the
example breakout game from the gweb examples:
https://github.com/life4/gweb/tree/master/examples/breakout

It makes use of the gweb API in that same repository.

# LICENSE

The LICENSE from the gweb repository continues to apply
to this source.

# Build steps

- Install Go
- make build

# Server for the game's files

- make serve

# Access game

- With a browser, visit http://localhost:33301/
