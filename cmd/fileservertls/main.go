// A basic HTTP file server.
package main

import (
	"flag"
	"log"
	"net/http"
)

func main() {
	listen := flag.String("listen", ":34000", "listen address")
	dir := flag.String("dir", ".", "directory to serve")
	certDirPrefix := flag.String("certdir", "", "cert & key directory")
	flag.Parse()

	if len(*certDirPrefix) == 0 {
		log.Fatalln("missing -certdir parameter")
	}

	certFile := *certDirPrefix + "/fullchain.pem"
	keyFile := *certDirPrefix + "/privkey.pem"

	log.Printf("listening on %q...", *listen)
	err := http.ListenAndServeTLS(*listen, certFile, keyFile, http.FileServer(http.Dir(*dir)))
	log.Fatalln(err)
}
