package main

import (
	"github.com/life4/gweb/canvas"
)

type Bricks struct {
	context      canvas.Context2D
	registry     []*Brick
	ready        bool
	windowWidth  int
	windowHeight int

	// stat
	score int
	hits  int
	text  *TextBlock
}

func (bricks *Bricks) Draw() {
	bricks.registry = make([]*Brick, BrickCols*BrickRows)
	usedWidth := (bricks.windowWidth / BrickCols) * BrickCols
	leftMargin := (bricks.windowWidth - usedWidth) / 2
	width := (bricks.windowWidth)/BrickCols - BrickMarginX
	colors := [...]string{"#e50c0c", "#de9b1f", "#25de1f", "#eadf43"}
	costs := [...]int{7, 5, 3, 1}
	for i := 0; i < BrickCols; i++ {
		for j := 0; j < BrickRows; j++ {
			x := leftMargin + (width+BrickMarginX)*i
			y := BrickMarginTop + (BrickHeight+BrickMarginY)*j
			color := colors[(j/2)%len(colors)]
			cost := costs[(j/2)%len(colors)]

			brick := Brick{
				context:   bricks.context,
				Rectangle: Rectangle{x: x, y: y, width: width, height: BrickHeight},
				cost:      cost,
			}
			brick.Draw(color)
			bricks.registry[BrickRows*i+j] = &brick
		}
	}
	bricks.ready = true
}

func (bricks *Bricks) Handle(ball *Ball) {
	if !bricks.ready {
		return
	}
	changed := false
	nHits := 0
	highCost := 0
	for _, brick := range bricks.registry {
		// we bounce the ball only on first collision with a brick in a frame
		if !brick.Collide(ball, !changed) {
			continue
		}
		// if the ball touched the brick, remove the brick and count score
		brick.Remove()
		bricks.score += brick.cost
		if brick.cost > highCost {
			highCost = brick.cost
		}
		bricks.hits += 1
		nHits++
		changed = true
	}
	if changed {
		// re-draw stat
		go bricks.text.DrawScore(bricks.score)
		go bricks.text.DrawHits(bricks.hits)

		// Only speed ball up after at least one bounce from the paddle/platform.
		// This gives less speed up of the ball if one "breaks through" the wall
		// and the ball bounces take out a lot of bricks before next paddle bounce.
		// The upper bricks are higher cost and knocking them out will contribute
		// to much less ball speed-up.
		if ball.platformBounce {
			ball.platformBounce = false
			// speed up ball after some hits
			ball.speedbumps++
			if ball.speedbumps >= HitsBetweenSpeedUps {
				ball.speedbumps = 0
				ball.vector.x += sign(ball.vector.x) * (SpeedUpFactorF * float64(highCost))
				ball.vector.y += sign(ball.vector.y) * (SpeedUpFactorF * float64(highCost))
				go bricks.text.DrawSpeed(ball.Speed())
			}
		}
	}
}

const SpeedUpFactorF = 0.04

func (bricks *Bricks) Count() int {
	count := 0
	for _, brick := range bricks.registry {
		if !brick.removed {
			count += 1
		}
	}
	return count
}

func sign(n float64) float64 {
	if n >= 0 {
		return 1
	}
	return -1
}
