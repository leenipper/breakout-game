package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/life4/gweb/web"
)

type Game struct {
	Width        int
	Height       int
	Window       web.Window
	Canvas       web.Canvas
	Panel1Y      int
	Panel2Y      int
	Panel1Height int
	Panel2Height int
	Body         web.HTMLElement

	state        *State
	platform     Platform
	ball         Ball
	block        TextBlock
	bricks       Bricks
	credits      int  // number of lost ball credits
	wallsCleared int  // # walls cleared
	demo         bool // letting the computer do the paddle movement
}

func (game *Game) Init() {
	game.state = &State{Stop: SubState{}}
	game.credits = InitialCredits
	game.wallsCleared = 0
	context := game.Canvas.Context2D()

	// draw play area background
	context.SetFillStyle(BGColor)
	context.BeginPath()
	context.Rectangle(0, game.Panel1Y, game.Width, game.Panel1Height).Filled().Draw()
	context.Fill()
	context.ClosePath()

	// draw Panel2 background
	context.SetFillStyle(BGColor2)
	context.BeginPath()
	context.Rectangle(0, game.Panel2Y, game.Width, game.Panel2Height).Filled().Draw()
	context.Fill()
	context.ClosePath()

	// make handlers
	rect := Rectangle{
		x:      game.Width / 2,
		y:      game.Panel1Height - 30,
		width:  PlatformWidth,
		height: PlatformHeight,
	}
	platformCircle := CircleFromRectangle(rect)
	game.platform = Platform{
		rect:           &rect,
		circle:         &platformCircle,
		circleInitialX: platformCircle.x,
		context:        context,
		element:        game.Canvas,
		initialX:       game.Width / 2,
		mouseX:         game.Width / 2,
		touchX:         game.Width / 2,
		windowWidth:    game.Width,
		windowHeight:   game.Panel1Height,
	}
	game.block = TextBlock{context: context, updated: time.Now(), textPosition: game.Panel2Y}
	game.ball = Ball{
		context:      context,
		windowWidth:  game.Width,
		windowHeight: game.Panel1Height,
		platform:     &game.platform,
	}
	game.ball.Reset(true)
	game.bricks = Bricks{
		context:      context,
		windowWidth:  game.Width,
		windowHeight: game.Panel1Height,
		ready:        false,
		text:         &game.block,
	}
	go game.bricks.Draw()
	game.block.drawNote("LeftClick to Play")
	game.block.DrawSpeed(game.ball.Speed())
	game.NewWallBallCredits()
	game.platform.DrawInitial()
	game.ball.Draw()
}

func (game *Game) NewWallBallCredits() {
	for c := 1; c <= game.credits; c++ {
		game.block.DrawBallCredit(c, false)
	}
}

func (game *Game) Demo(demoActive bool) {
	game.platform.Demo(demoActive)
}

func (game *Game) handler() {
	if game.state.Stop.Requested {
		game.state.Stop.Complete()
		return
	}

	var stopping bool

	wg := sync.WaitGroup{}
	wg.Add(5)
	go func() {
		// update FPS
		game.block.handle(game.credits)
		wg.Done()
	}()
	go func() {
		// update platform position
		game.platform.handleFrame()
		wg.Done()
	}()
	go func() {
		// check if the ball should bounce from a brick
		game.bricks.Handle(&game.ball)
		wg.Done()
	}()
	go func() {
		// check if the ball should bounce from border or platform
		game.ball.handle()
		wg.Done()
	}()
	go func() {
		// check if ball got out of playground
		if game.ball.y >= (game.Panel1Height + BallSize) {
			stopping = true
			// When out of credits, game is over.
			if game.credits == 0 {
				go game.fail()
			} else {
				game.block.DrawBallCredit(game.credits, true)
				game.credits -= 1
				game.ball.Reset(false)
				game.block.DrawSpeed(game.ball.Speed())
				// pause the game before launch of new ball
				go game.Stop()
			}
		}
		//
		if game.bricks.Count() == 0 {
			stopping = true
			game.wallsCleared++
			game.credits = InitialCredits
			if game.wallsCleared == 5 {
				// 5 walls cleared with only 3 balls is a win.
				// Top score is 2240.
				go game.win()
			} else {
				game.NewWallBallCredits()
				go game.NewWall()
			}
		}
		wg.Done()
	}()
	wg.Wait()
	if stopping {
		game.Window.Document().Call("exitPointerLock")
	}

	game.Window.RequestAnimationFrame(game.handler, false)
}

func (game *Game) Register(start bool) {
	game.state = &State{Stop: SubState{}}
	// register mouse movement handler
	game.Body.EventTarget().Listen(web.EventTypeMouseMove, game.platform.handleMouse)
	game.Body.EventTarget().Listen(web.EventType("touchmove"), game.platform.handleTouchMove)
	// register frame updaters
	if start {
		game.Window.RequestAnimationFrame(game.handler, false)
	}
}

func (game *Game) Stop() {
	if game.state.Stop.Completed {
		return
	}
	game.state.Stop.Request()
	game.state.Stop.Wait()
	game.block.drawNote("LeftClick to Continue")
}

func (game *Game) NewWall() {
	game.ball.eraseBall()
	game.ball.Reset(false) // Keeps same ball speed !
	game.block.DrawSpeed(game.ball.Speed())
	game.Stop()
	game.bricks.ready = false
	game.bricks.hits = 0
	game.bricks.Draw()
	game.block.drawNote("LeftClick to Continue")
}

func (game *Game) fail() {
	game.Stop()
	game.drawText("game-over score:"+strconv.Itoa(game.bricks.score), FailColor)
	game.block.drawNote("control-R for New game")
}

func (game *Game) win() {
	game.Stop()
	game.drawText("Score:"+strconv.Itoa(game.bricks.score), WinColor)
}

func (game *Game) drawText(text, color string) {
	height := TextHeight * 2
	width := TextWidth * 2
	context := game.Canvas.Context2D()
	context.Text().SetFont(fmt.Sprintf("bold %dpx Roboto", height))
	context.SetFillStyle(color)
	context.Text().Fill(text, (game.Width-width)/2, (game.Height-height)/2, width)
}
