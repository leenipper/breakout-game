package main

import (
	"math"
	"math/rand"

	"github.com/life4/gweb/canvas"
)

type Ball struct {
	Circle
	vector         Vector
	previousVector Vector

	windowWidth  int
	windowHeight int

	context        canvas.Context2D
	platform       *Platform
	ballColor      string
	speedbumps     int
	platformBounce bool
}

func (ball *Ball) BounceFromPoint(point Point) {

	normal := Vector{
		x: float64(point.x - ball.x),
		y: float64(point.y - ball.y),
	}
	normal = normal.Normalized()
	dot := ball.vector.Dot(normal)
	ball.vector = ball.vector.Sub(normal.Mul(2 * dot))
}

func (ball *Ball) changeDirection() {
	// right of the playground
	if ball.vector.x > 0 && ball.x > ball.windowWidth-ball.radius {
		ball.vector.x = -ball.vector.x
	}
	// left of the playground (with text box imagined all the way down)
	if ball.vector.x < 0 && ball.x < (ball.radius) {
		ball.vector.x = -ball.vector.x
	}

	// top of the playground
	if ball.vector.y < 0 && ball.y-ball.radius <= 0 {
		ball.vector.y = -ball.vector.y
	}

	// bounce from platform edges
	point := ball.platform.Touch(*ball)
	if point != nil {
		ball.BounceFromPoint(*point)
		ball.platformBounce = true
	}

	// Prevent left to right to left continuous bounce
	if math.Abs(ball.previousVector.y) < 0.5 && math.Abs(ball.vector.y) < 0.5 {
		ball.vector.y = 1
	}
	ball.previousVector = ball.vector
	ball.platform.NoteSpeed(ball.Speed())
}

func (ball *Ball) eraseBall() {
	ball.context.SetFillStyle(BGColor)
	ball.context.BeginPath()
	ball.context.Arc(ball.x, ball.y, ball.radius+1, 0, math.Pi*2)
	ball.context.Fill()
	ball.context.ClosePath()
}

func (ball *Ball) handle() {

	if ball.y < (ball.windowHeight - ball.radius) {
		// clear out previous render
		ball.eraseBall()
	}

	ball.changeDirection()

	// move the ball
	ball.x += int(math.Round(ball.vector.x))
	ball.y += int(math.Round(ball.vector.y))

	// draw new ball if not past bottom of Panel1
	if ball.y < (ball.windowHeight - ball.radius) {
		// draw the ball
		ball.Draw()
	}
}

func (ball *Ball) Draw() {
	ball.context.SetFillStyle(ball.ballColor)
	ball.context.BeginPath()
	ball.context.Arc(ball.x, ball.y, ball.radius, 0, math.Pi*2)
	ball.context.Fill()
	ball.context.ClosePath()
}

// Reset ball position and vector.
func (ball *Ball) Reset(newGame bool) {
	// set ball initial position
	ball.x = ball.platform.circle.x
	ball.y = ball.platform.rect.y - BallSize - 2
	// and size
	ball.radius = BallSize

	if newGame {
		// set ball initial vector
		//ball.vector = RandomInitialVector()
		ball.vector = RandomStartVector()
		ball.SetColor(InitialBallColor)
	} else {
		// Pick new direction but keep magnitude of ball.vector
		ball.vector = RandomNewDirection(ball.vector)
		// Reposition paddle(platform)
		ball.platform.rect.x = ball.platform.initialX
		ball.platform.mouseX = ball.platform.initialX
		// Reposition ball above paddle(platform)
		ball.x = ball.platform.circleInitialX
		ball.y = ball.platform.rect.y - BallSize - 2
	}
	ball.previousVector = ball.vector
}

func (ball *Ball) Speed() float64 {
	return math.Sqrt(ball.vector.x*ball.vector.x + ball.vector.y*ball.vector.y)
}

func (ball *Ball) SetColor(color string) {
	ball.ballColor = color
}

// initalVectorList contains various vectors with magnitudes of 7.62
var initialVectorList = [...]Vector{
	{x: -7.35, y: -2},
	{x: -7.00, y: -3},
	{x: -6.00, y: -4.7},
	{x: -6.50, y: -3.98},
	{x: -5.75, y: -5},
	{x: -5.00, y: -5.75},
	{x: -4.70, y: -6},
	{x: -3.98, y: -6.5},
	{x: -3.00, y: -7},
	{x: -2.00, y: -7.35},
	{x: -1.00, y: -7.55},
	{x: 1.00, y: -7.55},
	{x: 2.00, y: -7.35},
	{x: 3.00, y: -7},
	{x: 3.98, y: -6.5},
	{x: 4.70, y: -6},
	{x: 5.00, y: -5.75},
	{x: 5.75, y: -5},
	{x: 6.50, y: -3.98},
	{x: 6.00, y: -4.7},
	{x: 7.00, y: -3},
	{x: 7.35, y: -2},
}

var directionVectorList []Vector

// fillDirectionVectorList makes unit vectors of the initial vector list
func fillDirectionVectorList() {
	directionVectorList = make([]Vector, len(initialVectorList))
	for i, vector := range initialVectorList {
		m := math.Sqrt(vector.x*vector.x + vector.y*vector.y)
		directionVectorList[i].x = vector.x / m
		directionVectorList[i].y = vector.y / m
	}
}

func RandomInitialVector() (v Vector) {
	return initialVectorList[rand.Intn(len(initialVectorList))]
}

const InitialGameDifficulty = 7.62

var GameDifficulty = InitialGameDifficulty

func AdjustGameDifficulty(adjust float64) (ok bool) {
	d := GameDifficulty + adjust
	if adjust < 0 {
		if d >= (InitialGameDifficulty - 4.0) {
			GameDifficulty = d
			ok = true
		}
	} else {
		if d <= (InitialGameDifficulty + 16.0) {
			GameDifficulty = d
			ok = true
		}
	}
	return ok
}

func RandomStartVector() (v Vector) {
	m := GameDifficulty
	k := rand.Intn(len(directionVectorList))
	v.x = directionVectorList[k].x * m
	v.y = directionVectorList[k].y * m
	return v
}

// RandomNewDirection returns a new vector with same magnitude of old vector but new direction from fixed set
func RandomNewDirection(oldv Vector) (v Vector) {
	m := math.Sqrt(oldv.x*oldv.x + oldv.y*oldv.y)
	k := rand.Intn(len(initialVectorList))
	v.x = directionVectorList[k].x * m
	v.y = directionVectorList[k].y * m
	return v
}

func init() {
	fillDirectionVectorList()
}
