package main

import (
	"fmt"
	"math"
	"time"

	"github.com/life4/gweb/canvas"
)

type TextBlock struct {
	context          canvas.Context2D
	updated          time.Time
	displayedCredits int
	textPosition     int
}

const UseDebug = false

func (block TextBlock) drawFPS(now time.Time) {
	// calculate FPS
	if !UseDebug {
		fps := time.Second / now.Sub(block.updated)
		text := fmt.Sprintf("%d FPS", int64(fps))
		block.drawText(text, 1)
	}
}

func (block TextBlock) drawDebug(text string) {
	if UseDebug {
		block.drawText(text, 1)
	}
}

func (block TextBlock) drawNote(text string) {
	block.drawText(text, 0)
}

func (block *TextBlock) handle(credits int) {
	now := time.Now()
	// update FPS counter every second
	if block.updated.Second() != now.Second() {
		block.drawFPS(now)
		//block.drawDebug(fmt.Sprintf("%d", touchdebug))
	}
	block.updated = now
	if block.displayedCredits != credits {
		block.displayedCredits = credits
	}
}

func (block TextBlock) drawText(text string, column int) {
	x := TextLeft + column*(TextMargin+TextWidth)
	y := block.textPosition + TextTop

	// clear place where previous score was
	block.context.SetFillStyle(BGColor2)
	block.context.Rectangle(x, y, TextWidth+TextMargin, TextHeight+TextMargin).Filled().Draw()

	// draw the text
	block.context.SetFillStyle(TextColor)
	block.context.Text().SetFont(fmt.Sprintf("bold %dpx Roboto", TextHeight))
	block.context.Text().Fill(text, x, y+TextHeight, TextWidth)
}

func (block TextBlock) DrawScore(score int) {
	// make text
	var text string
	if score == 1 {
		text = fmt.Sprintf("%d point", score)
	} else {
		text = fmt.Sprintf("%d points", score)
	}
	block.drawText(text, 2)
}

func (block TextBlock) DrawHits(hits int) {
	// make text
	var text string
	if hits == 1 {
		text = fmt.Sprintf("%d hit", hits)
	} else {
		text = fmt.Sprintf("%d hits", hits)
	}
	block.drawText(text, 3)
}

func (block TextBlock) DrawSpeed(speed float64) {
	// make text
	var text string
	text = fmt.Sprintf(" %5.2f speed", speed)
	block.drawText(text, 4)
}

func (block TextBlock) DrawBallCredit(position int, erase bool) {
	x := TextLeft + TextColumns*(TextMargin+TextWidth) + position*(2*BallSize+TextMargin)
	y := block.textPosition + BallSize + BallSize/8
	if erase {
		block.context.SetFillStyle(BGColor2)
		block.context.BeginPath()
		block.context.Arc(x, y, BallSize+1, 0, math.Pi*2)
		block.context.Fill()
		block.context.ClosePath()
	} else {
		block.context.SetFillStyle(BallCreditColor)
		block.context.BeginPath()
		block.context.Arc(x, y, BallSize, 0, math.Pi*2)
		block.context.Fill()
		block.context.ClosePath()
	}

}
