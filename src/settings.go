package main

// colors
const (
	BGColor          = "#080802"
	BGColor2         = "#ccd0d1"
	InitialBallColor = "#f6f0f6"
	PlatformColor    = "#4283df"
	TextColor        = "#2c3e50"
	BallCreditColor  = TextColor
	FailColor        = "#c0392b"
	WinColor         = "#27ae60"
)

// platform
const (
	PlatformWidth    = 120
	PlatformHeight   = 20
	PlatformMaxSpeed = 30
	PlatformAura     = 5 // additional invisible bounce space around the platform
)

// ball
const BallSize = 16
const InitialCredits = 2

// bricks
const (
	BrickHeight     = 16
	BrickRows       = 8
	BrickCols       = 14
	BrickMarginLeft = 0  // pixels
	BrickMarginTop  = 70 // pixels
	BrickMarginX    = 5  // pixels
	BrickMarginY    = 5  // pixels
)

const SpeedUpFactor = 0.1     // 0.5
const HitsBetweenSpeedUps = 2 // 5

// These settings are not actually used now.
/*
var BrickSpeedUpHits = [...]int{15, 35, 55, 75, 95, 105}
var BallSpeedColors = [...]string{
	"#f3f7c1", "#f5faac", "#eadf43", "#25de1f", "#de9b1f", "#e50c0c",
}
*/

// text box
const (
	TextWidth   = 160
	TextHeight  = 14
	TextLeft    = 6
	TextTop     = 10
	TextMargin  = 5
	TextColumns = 5
)
