package main

import (
	"math/rand"
	"time"

	"github.com/life4/gweb/web"
)

var speedAdjustsOnlyNewWall = false

func main() {
	rand.Seed(time.Now().UnixNano())
	window := web.GetWindow()
	doc := window.Document()
	doc.SetTitle("Breakout")
	body := doc.Body()

	// create canvas
	h := window.InnerHeight() - 10
	w := window.InnerWidth() - 10
	canvas := doc.CreateCanvas()
	canvas.SetHeight(h)
	canvas.SetWidth(w)
	body.Node().AppendChild(canvas.Node())

	game := Game{
		Width:        w,
		Height:       h,
		Window:       window,
		Canvas:       canvas,
		Panel1Y:      0,
		Panel1Height: h - (h / 13),
		Panel2Y:      h - (h / 13) + 10,
		Panel2Height: h/13 - 10,
		Body:         body,
	}

	game.Init()
	//Try not starting game at load
	game.Register(false)
	// Attempt to start game with one mouse down event.
	game.state.Stop.Request()
	game.state.Stop.Complete()
	game.state.Stop.Wait()

	continueHandler := func(event web.Event) {
		game.block.drawNote("control-R:restart   P:pause")
		game.Canvas.RequestPointerLock()
		go func() {
			if game.state.Stop.Completed {
				game.Register(true)
			}
		}()
	}
	game.Body.EventTarget().Listen(web.EventTypeMouseDown, continueHandler)

	pauseResetKeyHandler := func(event web.Event) {
		go func() {
			keyCode := event.Get("keyCode").Int()
			// Demo key ?
			if keyCode == int('d') || keyCode == int('D') {
				game.Demo(true)
			} else if keyCode == int('e') || keyCode == int('E') {
				// On a fresh wall,allow E to adjust down the ball speed
				if game.state.Stop.StopIsComplete() && (!speedAdjustsOnlyNewWall || (game.bricks.Count() == (BrickCols * BrickRows))) {
					if ok := AdjustGameDifficulty(-1.0); ok {
						game.ball.Reset(true)
						game.block.DrawSpeed(game.ball.Speed())
					}
				}
			} else if keyCode == int('h') || keyCode == int('H') {
				// On a fresh wall, allow H to adjust up the ball speed
				if game.state.Stop.StopIsComplete() && (!speedAdjustsOnlyNewWall || (game.bricks.Count() == (BrickCols * BrickRows))) {
					if ok := AdjustGameDifficulty(1.0); ok {
						game.ball.Reset(true)
						game.block.DrawSpeed(game.ball.Speed())
					}
				}
				// Pause key ?
			} else if keyCode == int('p') || keyCode == int('P') {
				// Allow a pause
				game.Demo(false)
				if !game.state.Stop.Requested {
					game.Window.Document().Call("exitPointerLock")
					game.Stop()
				}
			}
		}()
	}
	game.Body.EventTarget().Listen(web.EventTypeKeyPress, pauseResetKeyHandler)

	// prevent ending of the program
	select {}
}
