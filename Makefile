# build file for breakout-game

PRJBASE := $(shell pwd)
PRJBIN  := $(PRJBASE)/bin
GOFILES := ./src/...

REPO_PATH := $(shell grep '^module ' ${PRJBASE}/go.mod | awk '{print $$2}')
VERSION := $(shell ${PRJBASE}/scripts/git-version.sh)
BUILD := $(shell git rev-parse --short HEAD)
GOROOT := $(shell go env GOROOT)
WASM_EXEC := $(shell ls ${GOROOT}/misc/wasm/wasm_exec.js)

GO_WASM := GOOS=js GOARCH=wasm 

SERVER_PORT ?= 33301
SERVER_PORT_TLS ?= 34200

.PHONY: default
default: help

## build: Build the binary
.PHONY: build
build: clean
	@echo VERSION $(VERSION)
	@mkdir -p $(PRJBIN)/server
	@cp $(PRJBASE)/frontend/* $(PRJBIN)/
	@cp ${WASM_EXEC} ${PRJBIN}/script.js
	${GO_WASM} go build -v -o $(PRJBIN)/frontend.wasm $(GOFILES)
	@go build -v -o $(PRJBIN)/server/server ./cmd/fileserver
	@go build -v -o $(PRJBIN)/server/servertls ./cmd/fileservertls

## clean: Clean build files. Runs 'go clean' internally.
.PHONY: clean
clean:
	@${GO_WASM} go clean
	@go clean
	@rm -rf ${PRJBIN}

## tidy: format/tidy source code
.PHONY: tidy
tidy:
	${GOENV} go mod tidy
	${GOENV} go fmt ./...

## serve: start server for game
.PHONY: serve
serve:
	${PRJBIN}/server/server  -dir ${PRJBIN} -listen :${SERVER_PORT} &

.PHONY: servetls
servetls:
	${PRJBIN}/server/servertls  -dir ${PRJBIN} -listen :${SERVER_PORT_TLS} &

## help: Print usage information
.PHONY: help
help: Makefile
	@echo
	@echo "Choose a command to run:"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' | sed -e 's/^/ /'
	@echo
