module gitlab.com/leenipper/breakout-game

go 1.16

require (
	github.com/life4/gweb v0.3.1
	github.com/stretchr/testify v1.7.0
)
